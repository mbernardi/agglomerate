Creating an algorithm
=====================

To create an algotithm you need to add your own class that has _Algorithm_ as
parent class.

An algorithm class has a ```supports``` dictionary, defining supported
features, and a ```pack()``` method.

Supports dictionary
-------------------

- rotation: whether the algorithm supports rotation of sprites
- cropping: True if the algorithm supports sprite cropping
- padding: True if the algorithm supports sprite padding
- auto_size: whether the algorithm supports deciding the size of the
	sheet
- auto_square_size: if the algorithm supports defining a squared sheet, 
	ignored if auto_size is False
- auto_power_of_two_size: if the algorithm supports defining a power-of-two
	sized sheet, ignored if auto_size is False

Pack method
-----------

Where the packing happens, the pack method must have two arguments: an
_items_ list and a _settings_ instance.

The _settings_ instance it is an instance of the _Settings_ class or the
_SheetSettings_ class, the algorithm must work with both, all the members you
need are in both classes anyway.

The algorithm must define a position to every item in _items_, also the
algorithm can crop, rotate or resize a sprite if you want to support that.

Also if you support that, you need to define the size of the bounding box
present in _settings_.

See the _inline_ algorihtm for a simple example
