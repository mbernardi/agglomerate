Item
:    Rectangle with position, size, etc. Sprites and Groups of sprites are
     items.

Sprite
:    Item that represent a sprite (image)

Group
:    Item that has a list of items, the items can be Sprites or more Groups.
     Also Groups have _Settings_, so every group can use a different algorithm
     that packs it's items.

Parameters
:    Special group, it's the root group, they have more settings, they use
     _SheetSettings_ instead of settings.

Settings
:    Class that has all the settings of a _Group_

SheetSettings
:    Extended settings class that has the settings for _Parameters_

Algorithm
:    Sets positions of the items

Format
:    Saves the items size and positions in a string, to save it on a file later
