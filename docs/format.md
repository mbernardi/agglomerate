Creating a format
=================

A format is a class that can read a list of sprites and return a string that
represent the position, size, etc. of every sprite.

See the _simplejson_ format as an example.

A format also has a _supports_ dictionary that lists the supported features and
a suggested file extension (e.g. .txt). 

Supports dictionary
-------------------

- rotation: whether the format supports rotation of sprites
- cropping: True if the format supports sprite cropping
