Settings
========

Keeps track of a group settings, this instance is for algorithms, can represent
an entire sheet or a group.

**Settings**
algorithm
:    name of the algorithm to use
allow
:    dictionary containing allowed settings
require
:    dictionary containing required settings
size
:    Vector2 that contains size of the generated sprite sheet image, values can
     be "auto"

**Allow dictionary**
rotation
:    True if the user allows the rotation of sprites
cropping
:    True if the user allows cropping of sprites

**Require dictionary**
square_size
:    True if a squared sheet is required
power_of_two_size
:    True if power-of-two dimensions are required
padding
:    Padding to apply to sprites, can be False or an integer


SheetSettings
-------------
Extended Settings that keeps track of all the sheet settings, this is valid
only for the entire sheet, groups use Settings instead. This object is used by
the packer and the formats. A Parameters instance contains a SheetSettings
instance.

Inherits the settings set in the Settings class and adds new ones:

**Added settings**
format
:    name of the coordinates file format
output_sheet_path
:    where to save the generated sprite sheet, if no extension is given,
     output_sheet_format is necessary, keep in mind that the saved file
     will lack extension
output_coordinates_path
:    where to save the generated coordinates file, if no extension is given
     no extension is added automatically, you can add one looking at the
     format suggested extension
output_sheet_format
:    image format used for saving. if None the format will be determined by
     the output_coordinates_path extension, this value is given to Pillow's
     Image.save() method, see Pillow documentation for more info
output_sheet_color_mode
:    color mode used for saving, this argument is given to Pillow's
     Image.new() method, see Pillow documentation for more info
background_color
:    color to use as the background of the sheet

**Tested output sheet image formats**
- None: determined from the output_sheet_path extension
- "png"
- "jpeg" ("jpg" doesn't work)
- "tiff"

**Tested output sheet color modes**
- "RGBA"
- "RGB"
- "CYMK" but messes colors, I don't know how it works
- "1"
- "L"
- "P" doesn't work, needs more arguments
